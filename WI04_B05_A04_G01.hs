{-Gruppe 01: Ajein Loganathan, Lennart Ferlings, Sebastian Hinrichs
Blatt 02, Abgabe: 18.11.2019
-}
-- Die folgende Zeile können Sie ignorieren (aber nicht löschen)
module P054 where

-- ------------------Aufgabenteil a-----------------

--Die Seite a ist konstant
a :: Float
a = 1.0

--Berechnet mit dem Satz des Pythagoras die Länge der Hypotenuse des n-ten Dreiecks
theoTriangle :: Int -> Float
theoTriangle n
                | n == 1 = sqrt(2.0)
                | n > 1 = sqrt(a^2 + (theoTriangle(n - 1))^2)

-- ------------------Aufgabenteil b-----------------

--Berechnet die Summe der Fläche aller n Dreiecke mit der Formel A= 0.5*g*h
theoArea :: Int -> Float
theoArea n 
            | n == 1 = 0.5
            | n > 1 = 0.5 * a * theoTriangle(n - 1) + theoArea(n - 1)

-- ------------------Aufgabenteil c-----------------

--Es fällt auf, dass bei jeder Berechnung Wurzeln das Ergebnis sind, die anschließend quadriert werden und mit 1 addiert.
--Somit ergibt sich für die Hypotenuse immer sqrt(n + 1)
theoSimple :: Int -> Float
theoSimple n  = sqrt(fromIntegral(n + 1))