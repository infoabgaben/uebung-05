{-Gruppe 01: Ajein Loganathan, Lennart Ferlings, Sebastian Hinrichs
Blatt 02, Abgabe: 18.11.2019
-}
-- Die folgende Zeile können Sie ignorieren (aber nicht löschen)
module P053 where

--Berechnet den Binominalkoeffizienten "n über k"
pascal :: Int -> Int -> Int
pascal n m
            | m == 0    = 1
            | m == n    = 1
            | m > n     = 0
            | otherwise = pascal (n - 1) (m - 1) + pascal (n - 1) m
            
--Führt man die Funktion mit den Parametern 3 und 1 aus entsteht folgende Rechnung
--pascal 3 1 = pascal 2 0 + pascal 2 1 = 1 + pascal 1 0 + pascal 1 1 = 1 + 1 + 1 = 3